'use strict';

if (!process.env.MSYMS_DIR) {
    console.log('Define MSYMS_DIR');
    process.exit(1);
}

const fs = require('fs')
const exec = require('child_process').exec;

const MSYM_DIR_PATH = process.env.MSYMS_DIR
let msymDirCache = {}
const getProductStructure = (productName) => {
    // TODO Support multiple products 
    if (!msymDirCache[productName]) {
        const configurations = fs.readdirSync(MSYM_DIR_PATH)
            .map(file => fs.statSync(`${MSYM_DIR_PATH}/${file}`).isDirectory() ? file : '')
            .filter(f => f)
        const productConfigurations = {}
        configurations.forEach(c => {
            const configPath = `${MSYM_DIR_PATH}/${c}`
            const configSubDirs = fs.readdirSync(configPath)
                .map(file => fs.statSync(`${configPath}/${file}`).isDirectory() ? file : '')
                .filter(f => f)
                .sort((a, b) => -a.localeCompare(b))
            productConfigurations[c] = configSubDirs
        })
        msymDirCache[productName] = productConfigurations
    }
    return msymDirCache[productName]
}

const writeTmpFilePromise = s => {
    const tempFilePath = '/tmp/msym-temp-contents.txt'
    return new Promise((f, r) => {
        fs.writeFile(tempFilePath, s, err => {
            if (err) {
                r(err)
            } else {
                f(tempFilePath)
            }
        });
    })
}

const execCmdPromise = cmd => {
    return new Promise((f, r) => {
        exec(cmd, (err, stdout, stderr) => {
            if (err) {
                r(err)
            } else {
                f({
                    out: stdout,
                    err: stderr
                })
            }
        });
    })
}

const Hapi = require('hapi');
const server = Hapi.server({
    port: process.env.PORT || 4574
});

(async function start() {
    await server.register(require('inert'));

    // Add the route
    server.route([{
        method: 'GET',
        path: '/',
        handler: function (request, h) {
            return h.file('index.html')
        }
    }, {
        method: 'GET',
        path: '/clear-cache',
        handler: function (request, h) {
            msymDirCache = {}
            return h.response().code(204)
        }
    },
    {
        method: 'GET',
        path: '/configuration/{product}',
        handler: function (request, h) {
            const productName = request.params.product
            if (request.query.clearCache || request.query['clear-cache']) {
                console.log("Clearing cache")
                msymDirCache = {}
            }
            try {
                const productStructure = getProductStructure(productName)
                return productStructure
            } catch (e) {
                console.error(e)
                return h.response().code(500)
            }
        }
    },
    {
        method: 'POST',
        path: '/mono-symbolicate/{product}/{configuration}/{version}',
        handler: function (request, h) {
            const stackTrace = request.payload.stackTrace

            if (stackTrace) {
                return writeTmpFilePromise(stackTrace)
                    .then(contentFilePath => {
                        const productName = request.params.product
                        const configuration = request.params.product
                        const version = request.params.version
                        const msymDirPath = `${MSYM_DIR_PATH}/${configuration}/${version}`
                        const cmd = `mono-symbolicate ${msymDirPath} ${contentFilePath}`
                        return execCmdPromise(cmd).then(output => {
                            const converted = output.out ? output.out : output.err
                            return converted
                        })
                    })
                    .catch(e => {
                        console.error(e)
                        return h.response().code(500)
                    })
            } else {
                return h.response('Define stacktrace').code(401)
            }
        }
    }, {
        method: 'GET',
        path: '/node/{param*}',
        handler: {
            directory: {
                path: 'node_modules'
            }
        }
    }]);

    try {
        await server.start();
        console.log('Server:', server.info.uri);
    }
    catch (err) {
        console.log(err);
        process.exit(1);
    }
})();